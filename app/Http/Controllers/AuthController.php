<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function register () {
        return view('register');
    }
    function welcome (Request $request) {
        $firstName = $request->FirstName;
        $lastName = $request->LastName;
        return view('welcome', compact('firstName','lastName'));
    }
}
